# Terraform AWS EC2 Instances
## Overview
This Terraform code allows you to create multiple EC2 instances in AWS. The number of instances can be adjusted by changing the value of the `instance_count` variable. Each instance will be launched with the specified AMI and instance type, and each instance will be tagged with a unique name based on the `instance_names` list.
## Prerequisites
Before running this Terraform code, make sure you have the following:
1. An AWS account with appropriate permissions to create EC2 instances.
2. Terraform installed on your local machine. You can download Terraform from the official website: [Terraform.io](https://www.terraform.io/downloads.html).
3. AWS CLI configured with valid access and secret keys.
## Usage
1. Clone this repository to your local machine or download the `main.tf` file.
2. Open the `main.tf` file in a text editor.
3. Customize the Terraform variables as needed:
   - `instance_count`: Adjust this variable to specify the number of EC2 instances you want to create. By default, it's set to `3`, but you can change it to any positive integer.
   - `region`: Set the desired AWS region in which you want to launch the EC2 instances. The default value is `us-west-2`, but you can change it to any other supported AWS region.
   - `ami`: Replace the placeholder `ami-12345678` with the AMI ID of the Amazon Machine Image you want to use for the instances.
   - `instance_type`: Choose the instance type for the EC2 instances. The default is `t2.micro`, but you can change it to any other supported instance type.
   - `instance_names`: Add or modify entries in this list if you change the `instance_count`. The instances will be tagged with names based on the order of the list.
4. Save the changes to the `main.tf` file.
5. Open a terminal or command prompt and navigate to the directory where the `main.tf` file is located.
6. Run the following commands:
`terraform init` initializes the working directory containing Terraform configuration files.
`terraform plan` creates an execution plan that shows what resources Terraform will create or modify.
`terraform apply` applies the changes and creates the EC2 instances based on the configuration.
7. When prompted, review the execution plan and type `yes` to confirm the creation of resources.
8. Wait for Terraform to provision the EC2 instances. Once completed, you will see the output with the instance details, such as public IP addresses and instance IDs.
## Cleanup
To clean up and destroy the resources created by Terraform, run the following command in the same directory where the `main.tf` file is located:
When prompted, review the execution plan and type `yes` to confirm the destruction of resources. This will terminate all the EC2 instances created by Terraform.
## Note
- Make sure to keep your AWS credentials secure and do not hardcode them in the Terraform configuration files.
- The `terraform.tfstate` file will be created in the same directory as the Terraform code. This file keeps track of the resources managed by Terraform. Do not delete or modify this file manually, as it may lead to Terraform losing track of the created resources.
- For more information on Terraform and AWS provider configuration, refer to the official Terraform documentation: [Terraform Documentation](https://www.terraform.io/docs/index.html) and [AWS Provider Documentation](https://registry.terraform.io/providers/hashicorp/aws/latest/docs).
- Always ensure that you have thoroughly reviewed and understood the changes that Terraform will apply before executing `terraform apply`. Incorrect configurations can lead to unintended resource creation or modification in your AWS account.
