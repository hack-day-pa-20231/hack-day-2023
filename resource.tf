resource "aws_instance" "ec2_instances" {
  count         = var.instance_count
  ami           = "ami-12345678"  # Replace with your desired AMI ID
  instance_type = "t2.micro"      # Replace with your desired instance type
  tags = {
    Name = local.instance_names[count.index]
  }
}
