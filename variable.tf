variable "instance_count" {
  default = 3  # Change this value to create a different number of instances
}

locals {
  instance_names = ["instance-1", "instance-2", "instance-3"]  # Add more names if you change the 'instance_count'
}
